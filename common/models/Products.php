<?php

namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;
use yii\web\UploadedFile;

class Products extends \yii\db\ActiveRecord
{
    public $productusers;

    public static function tableName()
    {
        return 'products';
    }

    public function rules()
    {
        return [
            [['code'], 'integer'],
            [['price', 'rec_price', 'min_price'], 'number'],
            [['code', 'name', 'price'], 'required'],
            [['description'], 'string'],
            [['created_on', 'updated_on', 'productusers'], 'safe'],
            [['name'], 'string', 'max' => 255],
            [['active', 'private'], 'string', 'max' => 1],
            [['module_id', 'code'], 'unique', 'targetAttribute' => ['module_id', 'code']],
            [['img'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg'],
            [['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => Category::className(), 'targetAttribute' => ['category_id' => 'id']],
            [['country_id'], 'exist', 'skipOnError' => true, 'targetClass' => Country::className(), 'targetAttribute' => ['country_id' => 'id']],
            [['module_id'], 'exist', 'skipOnError' => true, 'targetClass' => Modules::className(), 'targetAttribute' => ['module_id' => 'id']],
            [['treatment_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => ProductsTreatment::className(), 'targetAttribute' => ['treatment_type_id' => 'id']],
        ];
    }

    public function attributeLabels()
    {
        return [
//            'id' => 'ID',
            'code' => 'Код',
            'name' => 'Название',
            'description' => 'Описание',
            'img' => 'Изображение',
            'created_on' => 'Создан',
            'updated_on' => 'Обновлен',
            'country_id' => 'Страна',
            'module_id' => 'Модуль',
            'treatment_type_id' => 'Процесс',
            'category_id' => 'Категория',
            'price' => 'Цена',
        ];
    }

    public function getModule()
    {
        return $this->hasOne(Modules::className(), ['id' => 'module_id']);
    }

    public function getCountry()
    {
        return $this->hasOne(Country::className(), ['id' => 'country_id']);
    }

    public function getCategory()
    {
        return $this->hasOne(Category::className(), ['id' => 'category_id']);
    }

    public function getProcess()
    {
        return $this->hasOne(ProductsTreatment::className(), ['id' => 'treatment_type_id']);
    }

    public function getProductsUser()
    {
        return $this->hasMany(ProductsUser::className(), ['product_id' => 'id']);
    }

    public function getUsers()
    {
        return $this->hasMany(User::className(), ['id' => 'user_id'])->viaTable('products_user', ['product_id' => 'id']);
    }

    public function upload()
    {
        if ($this->validate()) {
            $this->img->name = hash('crc32', $this->img->baseName . time()) . '.' . $this->img->extension;
            $this->img->saveAs(Yii::$app->params['uploadPath'] . '/offers/' . $this->img->name);
            $this->img->tempName = Yii::$app->params['uploadPath'] . '/offers/' . $this->img->name;
            return true;
        } else {
            return false;
        }
    }

    public function beforeSave($insert)
    {
        if ($this->private == 1 && empty($this->productusers)) {
            \Yii::$app->session->setFlash('error', 'Для приватного оффера нужно выбрать пользователя!');
            return false;
        }
        return parent::beforeSave($insert);
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        ProductsUser::deleteAll(['product_id' => $this->id]);
        if ($this->private == 1) {
            if (is_array($this->productusers)) {
                foreach ($this->productusers as $user) {
                    $this->addProductUser($user);
                }
            }
        }
    }

    public function afterFind()
    {
        $this->productusers = ArrayHelper::map($this->users, 'id', 'id');
    }

    private function addProductUser($user_id)
    {
        $productUser = Yii::createObject([
                'class' => ProductsUser::className(),
                'user_id' => $user_id,
                'product_id' => $this->id,
            ]
        );
        if (!$productUser->save()) {
            return true;
        }
        return false;
    }
}
