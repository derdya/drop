<?php

namespace common\models;

class Orders extends \yii\db\ActiveRecord
{
    const ACCEPTED_STATUS = 2;
    public $productsInOrder = [];

    public static function tableName()
    {
        return 'orders';
    }

    public function rules()
    {
        return [
            [['productsInOrder', 'number'], 'safe'],
            [['phone'], 'match', 'pattern' => '/\([\d*]{3}\)[\d*]{3}\-[\d*]{2}\-[\d*]{2}/', 'message' => 'Заполните "правильно" поле: "Телефон клиента"'],
            [['number', 'seller_id', 'status_id', 'treatment_type_id', 'payment_type_id', 'delivery_id', 'module_id', 'date_create', 'date_update', 'date_send', 'send', 'total', 'country_id'], 'integer'],
            [['address', 'comment'], 'string'],
            [['client_name', 'phone', 'ttn', 'utm_source', 'utm_medium', 'utm_campaign', 'utm_term', 'utm_content'], 'string', 'max' => 255],
            [['ip_address'], 'string', 'max' => 20],
            [['country_id'], 'exist', 'skipOnError' => true, 'targetClass' => Country::className(), 'targetAttribute' => ['country_id' => 'id']],
            [['delivery_id'], 'exist', 'skipOnError' => true, 'targetClass' => Delivery::className(), 'targetAttribute' => ['delivery_id' => 'id']],
            [['module_id'], 'exist', 'skipOnError' => true, 'targetClass' => Modules::className(), 'targetAttribute' => ['module_id' => 'id']],
            [['payment_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => PaymentType::className(), 'targetAttribute' => ['payment_type_id' => 'id']],
            [['seller_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['seller_id' => 'id']],
            [['status_id'], 'exist', 'skipOnError' => true, 'targetClass' => Status::className(), 'targetAttribute' => ['status_id' => 'id']],
            [['treatment_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => OrdersTreatment::className(), 'targetAttribute' => ['treatment_type_id' => 'id']],

//            [['seller_id', 'status_id', 'treatment_type_id', 'number', 'total', 'payment_type_id', 'module_id'], 'safe'],
//            [['client_name', 'address', 'phone'], 'required'],
//            [['address', 'comment', 'utm_source', 'utm_medium', 'utm_campaign', 'utm_term', 'utm_content',], 'string'],
//            [['date_create', 'date_update', 'date_send', 'productsInOrder'], 'safe'],
//            [['client_name', 'ttn'], 'string', 'max' => 255],
//            [['phone'], 'match', 'pattern' => '/\([\d*]{3}\)[\d*]{3}\-[\d*]{2}\-[\d*]{2}/', 'message' => 'Заполните "правильно" поле: "Телефон клиента"'],
//            [['send'], 'string', 'max' => 1],
//            [['treatment_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => OrdersTreatment::className(), 'targetAttribute' => ['treatment_type_id' => 'id']],
//            [['seller_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['seller_id' => 'id']],
//            [['status_id'], 'exist', 'skipOnError' => true, 'targetClass' => Status::className(), 'targetAttribute' => ['status_id' => 'id']],
//            [['country_id'], 'exist', 'skipOnError' => true, 'targetClass' => Country::className(), 'targetAttribute' => ['country_id' => 'id']],
//            [['delivery_id'], 'exist', 'skipOnError' => true, 'targetClass' => Delivery::className(), 'targetAttribute' => ['delivery_id' => 'id']],
        ];
    }

    public function attributeLabels()
    {
        return [
            'date_create' => 'Дата созадния',
            'date_update' => 'Дата обновления',
            'total' => 'Сумма',
            'number' => 'Номер заказа',
            'client_name' => 'Имя клиента*:',
            'treatment_type_id' => 'Тип обработки:',
            'status_id' => 'Статус',
            'address' => 'Адрес*:',
            'comment' => 'Комментарий к заказу:',
            'phone' => 'Телефон клиента*:',
            'ttn' => 'ТТН',
            'send' => 'Отправить на обработку?',
            'country_id' => 'Страна:',
            'product' => 'Товары:',
            'productsList' => 'Список товаров:',
            'seller_id' => 'Продавец',
            'payment_type_id' => 'Тип расчета',
        ];
    }

    public function beforeSave($insert)
    {
        if (isset($_SESSION['productsInOrder']['productsOrder'])) $this->productsInOrder = @$_SESSION['productsInOrder']['productsOrder'];
        if (empty($this->productsInOrder)) {
            \Yii::$app->session->setFlash('error', 'Не возможно сохранить заказ без товара!');
            return false;
        }
        if (empty($this->country_id)) {
            $product = Products::find()->select('country_id')->where(['id' => $this->productsInOrder[0]['product_id']])->one();
            if (empty($product)) {
                die('Продукта с таким ID в БД не существует!');
            }
            $this->country_id = $product->country_id;
        }
        $this->ip_address = $_SERVER['REMOTE_ADDR'];
        if (empty($this->number)) {
            $this->number = number_format(round(microtime(true) * 10), 0, '.', '');
            $this->date_create = time();
        }
        $this->date_update = time();
        $this->seller_id = \Yii::$app->user->id;
        $this->status_id = self::ACCEPTED_STATUS;
        return parent::beforeSave($insert);
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        ProductsOrder::deleteAll(['order_id' => $this->id]);
        if (is_array($this->productsInOrder)) {
            foreach ($this->productsInOrder as $product) {
                $this->saveOrderProducts((object)$product);
            }
        }
        $this->clsProduct();
    }

    private function saveOrderProducts($product)
    {
//        var_dump($product);
//        die();
        $product = \Yii::createObject([
            'class' => ProductsOrder::className(),
            'product_id' => $product->product_id,
            'order_id' => $this->id,
            'name' => @$product->name,
            'quantity' => $product->quantity,
            'seller_price' => $product->seller_price,
        ]);
        if ($product instanceof ProductsOrder) {
            if ($product->save()) {
                return true;
            }
        }
        return false;
    }

    public function beforeDelete()
    {
        if (parent::beforeDelete()) {
            ProductsOrder::deleteAll(['order_id' => $this->id]);
            return true;
        } else {
            return false;
        }
    }

    public function addProduct($product)
    {
        $_SESSION['productsInOrder']['productsOrder'][] = $product;
        $this->productsInOrder = $_SESSION['productsInOrder']['productsOrder'];
    }

    public function delProduct($delete_id)
    {
        unset($this->productsInOrder[$delete_id]);
        unset($_SESSION['productsInOrder']['productsOrder'][$delete_id]);
    }

    public function afterFind()
    {
        parent::afterFind();
        if (isset($_SESSION['productsInOrder']['id']) && $_SESSION['productsInOrder']['id'] == $this->id) {
            $this->productsInOrder = $_SESSION['productsInOrder']['productsOrder'];
        } elseif (isset($_SESSION['productsInOrder'])) {
            unset($_SESSION['productsInOrder']);
            $_SESSION['productsInOrder'] = [
                'id' => $this->id,
                'productsOrder' => $this->productsOrder,
            ];
            $this->productsInOrder = $this->productsOrder;
        } else {
            $_SESSION['productsInOrder'] = [
                'id' => $this->id,
                'productsOrder' => $this->productsOrder,
            ];
            $this->productsInOrder = $this->productsOrder;
        }
    }

    public function clsProduct()
    {
        $this->productsInOrder = [];
        unset(\Yii::$app->session['productsInOrder']);
    }

    public function getOrdersTreatment()
    {
        return $this->hasOne(OrdersTreatment::className(), ['id' => 'treatment_type_id']);
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'seller_id']);
    }

    public function getModule()
    {
        return $this->hasOne(Modules::className(), ['id' => 'module_id']);
    }

    public function getStatus()
    {
        return $this->hasOne(Status::className(), ['id' => 'status_id']);
    }

    public function getCountry()
    {
        return $this->hasOne(Country::className(), ['id' => 'country_id']);
    }

    public function getProductsOrder()
    {
        return $this->hasMany(ProductsOrder::className(), ['order_id' => 'id']);
    }

    public function getProducts()
    {
        return $this->hasMany(Products::className(), ['id' => 'product_id'])->via('productsOrder');
    }

    public function getProductsList()
    {
        empty($this->country_id) ? $this->country_id = 1 : false;
        empty($this->module_id) ? $this->module_id = 1 : false;
        return Products::find()->where(['country_id' => $this->country_id, 'module_id' => $this->module_id])->all();
    }

    public function getDeliveryList()
    {
        empty($this->country_id) ? $this->country_id = 1 : false;
        return Delivery::find()->where(['country_id' => $this->country_id])->all();
    }

    public function getDelivery()
    {
        return $this->hasOne(Delivery::className(), ['id' => 'delivery_id']);
    }

    public function getPaymentType()
    {
        return $this->hasOne(PaymentType::className(), ['id' => 'payment_type_id']);
    }

    public function getTotal()
    {
        $total = 0;
        if ($this->productsInOrder) {
            foreach ($this->productsInOrder as $product) {
                $total += $product->total;
            }
        }
        return $total;
    }
}
