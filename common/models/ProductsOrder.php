<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "products_order".
 *
 * @property int $product_id
 * @property int $order_id
 * @property int $quantity
 * @property int $seller_price
 *
 * @property Orders $order
 * @property Products $product
 */
class ProductsOrder extends \yii\db\ActiveRecord
{
    public $name;

    public static function tableName()
    {
        return 'products_order';
    }

    public function rules()
    {
        return [
//            [['name'],'safe'],
            [['product_id', 'order_id', 'quantity', 'seller_price'], 'required'],
            [['product_id', 'order_id', 'quantity', 'seller_price'], 'integer'],
//            [['product_id', 'order_id'], 'unique', 'targetAttribute' => ['product_id', 'order_id']],
            [['order_id'], 'exist', 'skipOnError' => true, 'targetClass' => Orders::className(), 'targetAttribute' => ['order_id' => 'id']],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Products::className(), 'targetAttribute' => ['product_id' => 'id']],
        ];
    }

    public function attributeLabels()
    {
        return [
            'name' => 'Наименование:',
            'product_id' => 'Product ID',
            'order_id' => 'Order ID',
            'quantity' => 'Количество:',
            'seller_price' => 'Цена продажи:',
            'total' => 'Итог:',
        ];
    }


    public function getOrder()
    {
        return $this->hasOne(Orders::className(), ['id' => 'order_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Products::className(), ['id' => 'product_id']);
    }

    public function getTotal()
    {
        return $this->quantity * $this->seller_price;
    }


}
