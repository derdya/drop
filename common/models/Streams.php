<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "streams".
 *
 * @property int $id
 * @property string $uid
 * @property string $prices
 */
class Streams extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'streams';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['prices'], 'string'],
            [['uid'], 'string', 'max' => 255],
            [['uid'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'uid' => 'Uid',
            'prices' => 'Prices',
        ];
    }
}
