<?php

namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;
use common\models\Groups;

/**
 * This is the model class for table "menu".
 *
 * @property int $id id
 * @property int $parent_id Родитель
 * @property string $url Ссылка
 * @property string $icon
 * @property string $label Заголовок
 * @property int $sort Порядок
 * @property int $active
 */
class Menu extends \yii\db\ActiveRecord
{
    public $header = 'Меню';

    /**
     * {@inheritdoc}
     */


    public static function tableName()
    {
        return 'menu';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['parent_id', 'url', 'icon', 'label', 'sort'], 'required'],
            [['active'], 'required'],
            [['parent_id', 'sort', 'active', 'group_id'], 'integer'],
            [['url', 'label'], 'string', 'max' => 64],
            [['icon'], 'string', 'max' => 250],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'parent_id' => 'Родительская категория',
            'group_id' => 'Право доступа',
            'url' => 'URL',
            'icon' => 'Иконка',
            'label' => 'Название',
            'sort' => 'Сортировка',
            'active' => 'Активно',
        ];
    }

    public static function getMenu($access = 'admin')
    {
        $tree = [];
        $data = Menu::find()
            ->select(['id', 'parent_id', 'group_id', 'icon', 'url', 'label', 'sort', 'active AS locked'])
            ->asArray()
            ->indexBy('id')
            ->where(['active' => 1,])
            ->orderBy('sort')->with('group')
            ->all();

        foreach ($data as $id => &$node) {
            if (Yii::$app->user->can($node['group']['name'])) {
                if (isset($node['parent_id']) && !$node['parent_id']) {
                    $tree[$id] = &$node;
                } elseif (isset($node['parent_id'])) {
                    $data[$node['parent_id']]['items'][$node['id']] = &$node;
                    $data[$node['parent_id']]['items'][$node['id']]['label'] = '<i class="' . $node['icon'] . '"></i><span>' . $node['label'] . '</span>';
                }
            }
        }
        return $tree;
    }

    public function getParent()
    {
        return $this->hasOne(Menu::className(), ['id' => 'parent_id']);
    }


    public function getParentsList()
    {
        // Выбираем только те категории, у которых есть дочерние категории
        $parents = Menu::find()
            ->select(['id', 'label'])->where(['parent_id' => 0])->asArray()->all();
        $parents[] = ['id' => '0', 'label' => 'Родительская категория'];
        return ArrayHelper::map($parents, 'id', 'label');
    }

    public function getGroup()
    {
        return $this->hasOne(AuthItem::className(), ['id' => 'group_id']);
    }

    public function getGroups()
    {
        return AuthItem::find()->where(['type' => 1])->asArray()->all();
    }
}
