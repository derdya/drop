<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "modules_access_data".
 *
 * @property int $module_id
 * @property string $url
 * @property string $api_key
 * @property int $user_id
 *
 * @property Modules $module
 * @property User $user
 */
class ModulesAccessData extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'modules_access_data';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['module_id', 'url', 'api_key', 'user_id'], 'required'],
            [['module_id', 'user_id'], 'integer'],
            [['url', 'api_key'], 'string', 'max' => 250],
            [['module_id', 'url', 'user_id'], 'unique', 'targetAttribute' => ['module_id', 'url', 'user_id']],
            [['module_id'], 'exist', 'skipOnError' => true, 'targetClass' => Modules::className(), 'targetAttribute' => ['module_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'module_id' => 'Module ID',
            'url' => 'Url',
            'api_key' => 'Api Key',
            'user_id' => 'User ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getModule()
    {
        return $this->hasOne(Modules::className(), ['id' => 'module_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
