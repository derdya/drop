<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "track".
 *
 * @property int $id
 * @property string $date
 * @property string $ip_address
 * @property string $uid
 * @property string $stream
 * @property string $user_agent
 * @property int $timeonpage
 * @property int $transition
 * @property int $land_price
 */
class Track extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'track';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['uid'], 'required'],
            [['timeonpage', 'transition', 'land_price'], 'integer'],
            [['date', 'ip_address', 'uid', 'stream', 'user_agent'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'date' => 'Date',
            'ip_address' => 'Ip Address',
            'uid' => 'Uid',
            'stream' => 'Stream',
            'user_agent' => 'User Agent',
            'timeonpage' => 'Timeonpage',
            'transition' => 'Transition',
            'land_price' => 'Land Price',
        ];
    }
}
