<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "news".
 *
 * @property int $id
 * @property string $name
 * @property string $description
 * @property string $img
 * @property int $created_on
 * @property int $updated_on
 * @property int $active
 */
class News extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'news';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['description'], 'string'],
            [['created_on', 'updated_on'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['active'], 'string', 'max' => 1],
            [['img'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg'],
        ];
    }

    public function attributeLabels()
    {
        return [
//            'id' => 'ID',
            'name' => 'Название',
            'description' => 'Описание',
            'img' => 'Изображение',
            'created_on' => 'Создан',
            'updated_on' => 'Изменен',
            'active' => 'Активный',
        ];
    }

    public function upload()
    {
        if ($this->validate()) {
            $this->img->name = hash('crc32', $this->img->baseName . time()) . '.' . $this->img->extension;
            $this->img->saveAs(Yii::$app->params['uploadPath'] . '/news/' . $this->img->name);
            $this->img->tempName = Yii::$app->params['uploadPath'] . '/news/' . $this->img->name;
            return true;
        } else {
            return false;
        }
    }
}
