<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "country".
 *
 * @property int $id
 * @property string $name
 * @property string $code_geoip
 * @property int $delivery
 * @property int $return
 * @property int $currency_id
 *
 * @property Currency $currency
 */
class Country extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'country';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['delivery', 'return'], 'integer'],
            [['name'], 'string', 'max' => 50],
            [['code_geoip'], 'string', 'max' => 2],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'code_geoip' => 'Code Geoip',
            'delivery' => 'Delivery',
            'return' => 'Return',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCurrency()
    {
        return $this->hasMany(Currency::className(), ['currency_id' => 'id']);
    }
}
