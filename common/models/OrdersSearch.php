<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Orders;

/**
 * OrderSearch represents the model behind the search form of `common\models\Order`.
 */
class OrdersSearch extends Orders
{
    public $product;

    public function rules()
    {
        return [
            [['number', 'seller_id', 'status_id', 'treatment_type_id', 'country_id', 'total'], 'integer'],
            [
                [
                    'client_name',
                    'address',
                    'comment',
                    'phone',
                    'ttn',
                    'date_create',
                    'date_update',
                    'date_send',
                    'send',
                    'product',
                    'ip_address',
                    'utm_source',
                    'utm_medium',
                    'utm_campaign',
                    'utm_term',
                    'utm_content',
                ],
                'safe',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Orders::find()->joinWith([
            'user',
            'products',
            'ordersTreatment',
            'status',
            'country',
            'productsOrder',
        ]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        $dataProvider->sort->attributes['product'] = [
            'asc' => ['products.id' => SORT_ASC],
            'desc' => ['products.id' => SORT_DESC],
        ];
        $this->load($params);
        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'seller_id' => $this->seller_id,
            'status_id' => $this->status_id,
            'orders.treatment_type_id' => $this->treatment_type_id,
            'date_create' => $this->date_create,
            'date_send' => $this->date_send,
        ]);
        $query->andFilterWhere(['like', 'client_name', $this->client_name])
            ->andFilterWhere(['like', 'address', $this->address])
            ->andFilterWhere(['like', 'number', $this->number])
            ->andFilterWhere(['like', 'comment', $this->comment])
            ->andFilterWhere(['like', 'phone', $this->phone])
            ->andFilterWhere(['like', 'ttn', $this->ttn])
            ->andFilterWhere(['like', 'send', $this->send])
            ->andFilterWhere(['like', 'ip_address', $this->ip_address])
            ->andFilterWhere(['like', 'products.name', $this->product])
            ->andFilterWhere(['like', 'utm_source', $this->utm_source])
            ->andFilterWhere(['like', 'utm_medium', $this->utm_medium])
            ->andFilterWhere(['like', 'utm_campaign', $this->utm_campaign])
            ->andFilterWhere(['like', 'utm_term', $this->utm_term])
            ->andFilterWhere(['like', 'utm_content', $this->utm_content])
            ->andFilterWhere(['like', 'total', $this->total]);

        return $dataProvider;
    }
}
