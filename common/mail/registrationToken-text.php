<?php

/* @var $this yii\web\View */
/* @var $user common\models\User */

$resetLink = Yii::$app->urlManager->createAbsoluteUrl(['site/confirm', 'authKey' => $user->auth_key]);
?>
Привет <?= $user->username ?>,

Перейдите по ссылке что-бы подтвердить регистрацию на сайте: http://Dropplatforma.ru/

<?= $resetLink ?>
