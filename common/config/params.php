<?php
return [
    'adminEmail' => 'admin@example.com',
    'supportEmail' => 'support@dropplatforma.ru',
    'user.passwordResetTokenExpire' => 3600,
    'uploadPath' => realpath(dirname(__FILE__).'/../uploads/images/')
];
