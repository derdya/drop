<?php
return [
    'buttonSave' => 'Сохранить',
    'buttonUpdate' => 'Редактировать',
    'buttonDelete' => 'Удалить',
    'msgDelConfirm' => 'Вы уверены, что хотите удалить этот элемент?',

];