<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user common\models\User */

$resetLink = Yii::$app->urlManager->createAbsoluteUrl(['site/confirm', 'authKey' => $user->auth_key]);
?>
<div class="password-reset">
    <p>Привет <?= Html::encode($user->username) ?>,</p>

    <p>Перейдите по ссылке что-бы подтвердить регистрацию на сайте: <?= Html::a('Dropplatforma.ru','http://Dropplatforma.ru/')?></p>

    <p><?= Html::a(Html::encode($resetLink), $resetLink) ?></p>
</div>