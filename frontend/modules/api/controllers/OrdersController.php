<?php
/**
 * Created by PhpStorm.
 * User: fbp
 * Date: 30.05.2018
 * Time: 10:52
 */

namespace frontend\modules\api\controllers;

use common\models\Orders;
use common\models\OrdersSearch;
use common\models\Products;
use common\models\ProductsSearch;
use common\models\User;
use Yii;
use yii\data\ActiveDataFilter;
use yii\data\ActiveDataProvider;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\QueryParamAuth;
use yii\web\Response;
use yii\rest\ActiveController;

class OrdersController extends ActiveController
{
    public $modelClass = 'common\models\Orders';
    private $dataFilter;

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['contentNegotiator'] = [
            'class' => \yii\filters\ContentNegotiator::className(),
            'formats' => [
                'application/json' => \yii\web\Response::FORMAT_JSON,
                'application/xml' => \yii\web\Response::FORMAT_XML,
            ],
        ];
        $behaviors['authenticator'] = [
            'class' => QueryParamAuth::className(),
            'tokenParam' => 'auth_key',
        ];
        Yii::$app->user->enableSession = false;
        return $behaviors;
    }

    public function actions()
    {
        $actions = parent::actions();

//         disable the "delete" and "create" actions

        $actions['index']['prepareDataProvider'] = [$this, 'prepareDataProvider'];
        unset($actions['view']);
//        unset($actions['create']);
        return $actions;
    }


    public function prepareDataProvider()
    {
        $filter = new ActiveDataFilter([
            'searchModel' => 'common\models\ProductsSearch'
        ]);
        $filterCondition = null;
        if ($filter->load(\Yii::$app->request->post())) {
            $filterCondition = $filter->build();
            if ($filterCondition === false) {
                return $filter;
            }
        }

        $query = $this->modelClass::find();
        if ($filterCondition !== null) {
            $query->andWhere($filterCondition);
        }

        return new ActiveDataProvider([
            'query' => $query,
        ]);
    }


    public function actionView()
    {
        $request = null;
        if (Yii::$app->request->post()) {
            $request = Yii::$app->request->post();
        } elseif (Yii::$app->request->get()) {
            $request = Yii::$app->request->get();
        }
        $user = $this->checkAcess($request['auth_key']);
        if (!isset($request['number'])) {
            return 'Запрос должен содержать номер заказа "number"';
        }
        if ($user) {
            return $this->modelClass::find()->where(['seller_id' => $user->getId(), 'number' => $request['number']])->one();
        }
        return 'Запрос должен содержать "auth_key"';
    }

//    private function actionCreate()
//    {
//        return 'lol';
//    }


}