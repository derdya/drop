<?php
$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

return [
    'id' => 'app-frontend',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'frontend\controllers',
    'as AccessBehavior' => [
        'class' => \developeruz\db_rbac\behaviors\AccessBehavior::className(),
        'rules' => [
            'site' => [
                [
                    'allow' => true,
                    'roles' => ['?'],
                ],
            ],
            'api/orders' => [
                [
                    'allow' => true,
                    'roles' => ['?', '@'],
                ],
            ],
        ],
        'redirect_url' => '/',
        'login_url' => '/site/login',
    ],
    'modules' => [
        'api' => [
            'class' => 'frontend\modules\api\Api',
        ],
    ],
    'components' => [
        'assetManager' => [
            'bundles' => [
                'frontend\assets\AdminLteAsset' => [
                    'skin' => 'skin-purple',
                ],
            ],
        ],
        'request' => [
            'baseUrl' => '',
            'csrfParam' => '_csrf-site',
            'parsers' => [
                'application/json' => 'yii\web\JsonParser',
            ],
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
            'identityCookie' => [
                'name' => '_identity-infosite',
                'httpOnly' => true,
            ],

        ],
        'session' => [
            // this is the name of the session cookie used for login on the frontend
            'name' => 'info-site',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => 'api\orders',
                    'pluralize' => false,
                ],
                'products/all/<id:\d+>' => 'products/one',
                'products/<all:\w+>' => 'products/all',

                '' => 'site/index',
                '<controller:\w+>/<action:\w+>/' => '<controller>/<action>',

            ],
        ],

    ],
    'params' => $params,
];
