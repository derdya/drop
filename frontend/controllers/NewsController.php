<?php

namespace frontend\controllers;

use common\models\News;
use common\models\NewsSearch;
use yii\web\NotFoundHttpException;

class NewsController extends \yii\web\Controller
{

    public function actionShow()
    {
        $searchModel = new NewsSearch();
        $dataProvider = $searchModel->search(\Yii::$app->request->queryParams);
        if (\Yii::$app->request->isAjax) {
            return $this->renderAjax('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
        }
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionOne($id)
    {
        if (\Yii::$app->request->isAjax) {
            return $this->renderAjax('oneView', [
                'model' => $this->findModel($id),
            ]);
        }
        return $this->render('oneView', [
            'model' => $this->findModel($id),
        ]);
    }

    protected function findModel($id)
    {
        if (($model = News::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
