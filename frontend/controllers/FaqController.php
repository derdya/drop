<?php

namespace frontend\controllers;

class FaqController extends \yii\web\Controller
{
    public function actionCommon()
    {
        return $this->render('common');
    }
    public function actionDomain()
    {
        return $this->render('domain');
    }
    public function actionOverview()
    {
        return $this->render('overview');
    }



}
