<?php

namespace frontend\controllers;

use common\models\Status;

class DashboardController extends \yii\web\Controller
{
    public function actionIndex()
    {
        $statusBox = Status::find()->where(['active' => 1])->all();
        if (\Yii::$app->request->isAjax) {
            return $this->renderAjax('index', ['statusBox' => $statusBox]);
        }
        return $this->render('index', ['statusBox' => $statusBox]);
    }

}
