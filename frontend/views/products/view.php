<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

$this->title = 'Оффер ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Офферы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$this->params['buttons'] = [];
?>

<?= DetailView::widget([
    'model' => $model,
    'attributes' => [
        'id',
        'code',
        'name',
        [
            'attribute' => 'img',
            'label' => 'Картинка',
            'format' => 'raw',
            'value' => function ($data) {
                return Html::img('/common/uploads/images/offers/' . $data->img, [
                    'alt' => 'yii2 - картинка в gridview',
                    'style' => 'width:100px;',
                ]);
            },
            'filter' => false,
        ],
        'description:ntext',

        [
            'attribute' => 'created_on',
            'label' => 'Создано',
            'format' => 'text',
            'value' => function ($data) {
                return Yii::$app->formatter->asDate($data->created_on, 'yyyy-MM-dd H:i:s');
            },
            'filter' => false,
        ],
        [
            'attribute' => 'updated_on',
            'label' => 'Обновлено',
            'format' => 'text',
            'value' => function ($data) {
                return Yii::$app->formatter->asDate($data->updated_on, 'yyyy-MM-dd H:i:s');
            },
            'filter' => false,
        ],

    ],
]) ?>

