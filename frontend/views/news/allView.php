<?

use \yii\helpers\Html;
?>
<div class="media" style="margin-bottom: 25px">
    <div class="media-left">
        <?= Html::a(
            Html::img(
                "/common/uploads/images/news/{$model->img}",
                ['style' => 'width: 200px;']
            ),
            [
                '/news/one/', 'id' => $model->id,
            ]
        ) ?>
    </div>
    <div class="media-body">
        <h4 class="media-heading"><?= Html::a($model->name, ['/news/one/', 'id' => $model->id]) ?></h4>
        <div style="margin-bottom: 10px">
            <i class="fa fa-calendar"></i> <?= Yii::$app->formatter->asDate($model->updated_on, 'dd-MM-Y H:i'); ?>
        </div>
        <div style="font-size: 14px"><p><?= $model->description ?></p></div>
    </div>
</div>
