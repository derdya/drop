<?

use yii\widgets\Breadcrumbs;
use yii\helpers\Html;

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Новости', 'url' => ['show']];
$this->params['breadcrumbs'][] = ['label' => $this->title];

?>
<div class="row">
    <div class="col-xs-12 col-md-12 col-sm-12">
        <div class="box box-primary" style="overflow-x: auto">
            <div class="box-header with-border">
                <div class="box-title">
                    <?= Breadcrumbs::widget(
                        [
                            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                        ]
                    ) ?>
                </div>
                <div class="box-tools pull-right">
                    <div class="box-tools pull-right">
                        <? if (isset($this->params['buttons']) && !empty($this->params['buttons'])) {
                            foreach ($this->params['buttons'] as $button) {
                                echo $button;
                            }
                        } ?>
                    </div>
                </div>
            </div>
            <div class="box-body">
                <section class="content">
                    <div class="media-left">
                        <?= Html::a(
                            Html::img(
                                "/common/uploads/images/news/{$model->img}",
                                ['style' => 'width: 200px;']
                            ),
                            [
                                '/news/one/', 'id' => $model->id,
                            ]
                        ) ?>
                    </div>
                    <?= $model->description ?>
                </section>
            </div>
        </div>
    </div>
</div>