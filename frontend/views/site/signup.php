<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Signup';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="register-box">
    <div class="register-logo">
        <a href=""><b>Drop</b>Platforma</a>
    </div>

    <div class="register-box-body">
        <p class="login-box-msg">Регистрация</p>
        <?php $form = ActiveForm::begin(['id' => 'form-signup']); ?>

        <div class="form-group has-feedback">
            <?= $form->field($model, 'username')->textInput(
                [
                    'autofocus' => true,
                    'placeholder' => 'Имя',
                    'class' => 'form-control',
                ]
            )->label(false) ?>
            <span class="glyphicon glyphicon-user form-control-feedback"></span>
        </div>
        <div class="form-group has-feedback">
            <?= $form->field($model, 'email')->textInput([
                'placeholder' => 'email',
                'class' => 'form-control',
            ])->label(false) ?>
            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
        </div>
        <div class="form-group has-feedback">
            <?= $form->field($model, 'password')->passwordInput(
                [
                    'placeholder' => 'Пароль',
                    'class' => 'form-control',
                ]
            )->label(false) ?>
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
        </div>
        <div class="form-group has-feedback">
            <?= $form->field($model, 'password_repeat')->passwordInput(
                [
                    'placeholder' => 'Подтверждение пароля',
                    'class' => 'form-control',
                ]
            )->label(false) ?>
            <span class="glyphicon glyphicon-log-in form-control-feedback"></span>
        </div>
        <div class="row">
            <div class="col-xs-6">
                <!--                    <div class="checkbox icheck">-->
                <label>
                    <?= $form->field($model, 'agree')->checkbox([
                        'label' => 'Я согласен с ' . Html::a('правилами', '/'),
                    ]) ?>
                    <!--                            <input type="checkbox"> Я согласен с <a href="#">Правилами</a>-->
                </label>
                <!--                    </div>-->
            </div>
            <div class="col-xs-6">
                <?= Html::submitButton('Подтвердить', ['class' => 'btn btn-primary btn-block btn-flat', 'name' => 'signup-button']) ?>
            </div>
            <!-- /.col -->
        </div>
        <?php ActiveForm::end(); ?>


        <!--        <div class="social-auth-links text-center">-->
        <!--            <p>- OR -</p>-->
        <!--            <a href="#" class="btn btn-block btn-social btn-facebook btn-flat"><i class="fa fa-facebook"></i> Sign up-->
        <!--                using-->
        <!--                Facebook</a>-->
        <!--            <a href="#" class="btn btn-block btn-social btn-google btn-flat"><i class="fa fa-google-plus"></i> Sign up-->
        <!--                using-->
        <!--                Google+</a>-->
        <!--        </div>-->

        <a href="/login" class="text-center">Уже зарегистрирован.</a>
    </div>
    <!-- /.form-box -->
</div>

