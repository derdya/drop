<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$fieldOptions1 = [
    'options' => ['class' => 'form-group has-feedback'],
    'inputTemplate' => "{input}<span class='glyphicon glyphicon-envelope form-control-feedback'></span>",
];

$fieldOptions2 = [
    'options' => ['class' => 'form-group has-feedback'],
    'inputTemplate' => "{input}<span class='glyphicon glyphicon-lock form-control-feedback'></span>",
];
?>

<div class="login-box">
    <div class="login-logo">
        <a href="#"><b>Drop</b>Platforma</a>
    </div>
    <!-- /.login-logo -->
    <div class="login-box-body">
        <?= \common\widgets\Alert::widget(); ?>
        <p class="login-box-msg">Войдите, чтобы начать работать.</p>

        <?php $form = ActiveForm::begin(['id' => 'login-form', 'enableClientValidation' => false]); ?>

        <?= $form
            ->field($model, 'email', $fieldOptions1)
            ->label(false)
            ->textInput(['placeholder' => $model->getAttributeLabel('email')]) ?>

        <?= $form
            ->field($model, 'password', $fieldOptions2)
            ->label(false)
            ->passwordInput(['placeholder' => 'Пароль']) ?>

        <div class="row">
            <div class="col-xs-8">
                <div class="checkbox icheck">
                    <label>
                        <?= $form->field($model, 'rememberMe')->checkbox()->label('Запоминть меня') ?>
                    </label>
                </div>
            </div>
            <!-- /.col -->
            <div class="col-xs-4">
                <?= Html::submitButton('Войти', ['class' => 'btn btn-primary btn-block btn-flat', 'name' => 'login-button']) ?>
            </div>
            <!-- /.col -->
        </div>


        <?php ActiveForm::end(); ?>

        <!--        <div class="social-auth-links text-center">-->
        <!--            <p>- OR -</p>-->
        <!--            <a href="#" class="btn btn-block btn-social btn-facebook btn-flat"><i class="fa fa-facebook"></i> Sign in-->
        <!--                using Facebook</a>-->
        <!--            <a href="#" class="btn btn-block btn-social btn-google-plus btn-flat"><i class="fa fa-google-plus"></i> Sign-->
        <!--                in using Google+</a>-->
        <!--        </div>-->
        <!-- /.social-auth-links -->
        <div class="row">
            <div class="col-xs-6 col-md-6">
                <?= Html::a('Я забыл мой пароль',['/site/request-password-reset'],['data-method' => 'get']); ?>
            </div>
            <div class="col-xs-6 col-md-6">
                <?= Html::a(
                    'Зарегистрироваться',
                    ['/site/signup'],
                    ['data-method' => 'get']
                ) ?>
            </div>
        </div>
    </div>
    <!-- /.login-box-body -->
</div><!-- /.login-box -->
