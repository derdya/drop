<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Cброс пароля';
?>
<div class="login-box">
    <div class="login-logo">
        <h1><?= Html::encode($this->title) ?></h1>
    </div>
    <div class="login-box-body">
        <p class="login-box-msg">
            Пожалуйста, введите свой новый пароль.
        </p>
        <?php $form = ActiveForm::begin(['id' => 'reset-password-form']); ?>
        <?= $form->field($model, 'password')->passwordInput(['autofocus' => true, 'placeholder' => 'Пароль'])->label(false) ?>
        <?= $form->field($model, 'password_repeat')->passwordInput(['placeholder' => 'Подтверждение пароля', 'class' => 'form-control',])->label(false) ?>
        <div class="form-group">
            <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary', 'style' => 'width:100%;']) ?>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>