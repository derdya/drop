<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Запрос на сброс пароля';
?>
<div class="login-box">
    <div class="login-logo">
        <h1><?= Html::encode($this->title) ?></h1>
    </div>
    <div class="login-box-body">
        <p class="login-box-msg">
            Пожалуйста, введите свой адрес электронной почты. Вам будет отправлена ​​ссылка на сброс пароля.
        </p>
        <?php $form = ActiveForm::begin(['id' => 'request-password-reset-form']); ?>
        <?= $form->field($model, 'email')->label(false)->textInput(['autofocus' => true, 'placeholder' => 'email']) ?>
        <div class="form-group">
            <?= Html::submitButton('Отправить', ['class' => 'btn btn-primary', 'style' => 'width:100%;']) ?>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>
