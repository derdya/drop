<?php

/* @var $this yii\web\View */

use dosamigos\chartjs\ChartJs;

$this->title = 'My Yii Application';
?>
<div class="site-index">
    <div class="body-content">
        <div class="col-lg-3 col-xs-6">
            <div class="small-box " style="background:red">
                <div class="inner">
                    <h3>Тест</h3>
                    <p>Тест</p>
                </div>
                <div class="icon">
                    <i class="ion ion-bag"></i>
                </div>
                <a href="" class="small-box-footer">Инфо. <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>
        <div class="row">


            <?= ChartJs::widget([
                'type' => 'line',
                'options' => [
                    'height' => 100,
                    'width' => 400,
                ],
                'data' => [
                    'labels' => ["January", "February", "March", "April", "May", "June", "July"],
                    'datasets' => [
                        [
                            'label' => "Тест 1",
                            'backgroundColor' => "rgba(179,181,198,0.2)",
                            'borderColor' => "rgba(179,181,198,1)",
                            'pointBackgroundColor' => "rgba(179,181,198,1)",
                            'pointBorderColor' => "#fff",
                            'pointHoverBackgroundColor' => "#fff",
                            'pointHoverBorderColor' => "rgba(179,181,198,1)",
                            'data' => [65, 59, 90, 81, 56, 55, 40],
                        ],
                        [
                            'label' => "Тест 2",
                            'backgroundColor' => "rgba(255,99,132,0.2)",
                            'borderColor' => "rgba(255,99,132,1)",
                            'pointBackgroundColor' => "rgba(255,99,132,1)",
                            'pointBorderColor' => "#fff",
                            'pointHoverBackgroundColor' => "#fff",
                            'pointHoverBorderColor' => "rgba(255,99,132,1)",
                            'data' => [43, 76, 89, 12, 13, 98, 55],
                        ],
                        [
                            'label' => "Тест 3",
                            'backgroundColor' => "rgba(255,99,132,0.2)",
                            'borderColor' => "rgba(255,99,132,1)",
                            'pointBackgroundColor' => "rgba(255,99,132,1)",
                            'pointBorderColor' => "#fff",
                            'pointHoverBackgroundColor' => "#fff",
                            'pointHoverBorderColor' => "rgba(255,99,132,1)",
                            'data' => [89, 12, 89, 11, 12, 87, 1],
                        ],
                        [
                            'label' => "Тест 4",
                            'backgroundColor' => "rgba(255,99,132,0.2)",
                            'borderColor' => "rgba(255,99,132,1)",
                            'pointBackgroundColor' => "rgba(255,99,132,1)",
                            'pointBorderColor' => "#fff",
                            'pointHoverBackgroundColor' => "#fff",
                            'pointHoverBorderColor' => "rgba(255,99,132,1)",
                            'data' => [33, 48, 1, 99, 44, 23, 22],
                        ],

                    ],
                ],
            ]); ?>
        </div>
    </div>
</div>
