<? foreach ($statusBox as $box): ?>
    <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <div class="small-box" style="background:<?= $box->color ?>">
            <div class="inner">
                <h3>150</h3>

                <p><?= $box->name ?></p>
            </div>
            <div class="icon">
                <i class="ion ion-bag"></i>
            </div>
            <a href="#" class="small-box-footer">Инфо. <i class="fa fa-arrow-circle-right"></i></a>
        </div>
    </div>
<? endforeach; ?>

