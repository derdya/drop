<?php

use yii\widgets\Breadcrumbs;
use miloschuman\highcharts\Highcharts;

$this->title = 'Панель статистики';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row">
    <?= $this->render('statusBox.php', ['statusBox' => $statusBox]) ?>
</div>
<div class="row">
    <div class="col-sm-12">
        <?
        function generateData($statusBox)
        {
            foreach ($statusBox as $status) {
                $status = ['name' => $status->name];
                for ($i = 0; $i < 30; $i++) {
                    $status['data'][] = rand(1, 100);
                }
                $dataChart[] = $status;
            }
            return $dataChart;
        }

        ?>
        <?= Highcharts::widget([
            'options' => [
                'credits' => ['enabled' => false],
                'title' => ['text' => 'Статистика(Инфограмма)'],
//                        'xAxis' => [
////                            'categories' => ['Apples', 'Bananas', 'Oranges'],
//                        ],
                'yAxis' => [
                    'title' => ['text' => 'Статистика(Инфограмма)'],
                ],
                'series' => generateData($statusBox),
            ],
        ]); ?>
    </div>
</div>


