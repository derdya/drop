<?php

namespace backend\controllers;

use backend\models\ServerControl;
use common\models\Streams;
use common\models\Track;
use common\models\User;
use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;

class AdminController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionIndex()
    {
        $pids = ServerControl::getAllPids();
        return $this->render('index', compact('pids'));
    }

    public function actionServer()
    {

        if (Yii::$app->request->isPjax) {
            $command = Yii::$app->request->post('command');
            $type = Yii::$app->request->post('type');
            switch ($command) {
                case 'start':
                    ServerControl::startServer($type);
                    break;
                case 'stop':
                    ServerControl::stopServer($type);
                    break;
            }
            $pids = ServerControl::getAllPids();
            return $this->renderAjax('index', compact('pids'));
        }
        return false;
    }

    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            $model->password = '';

            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }
}


