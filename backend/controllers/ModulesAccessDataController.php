<?php

namespace backend\controllers;

use Yii;
use common\models\ModulesAccessData;
use common\models\ModulesAccessDataSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ModulesAccessDataController implements the CRUD actions for ModulesAccessData model.
 */
class ModulesAccessDataController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all ModulesAccessData models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ModulesAccessDataSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single ModulesAccessData model.
     * @param integer $module_id
     * @param string $url
     * @param integer $user_id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($module_id, $url, $user_id)
    {
        return $this->render('view', [
            'model' => $this->findModel($module_id, $url, $user_id),
        ]);
    }

    /**
     * Creates a new ModulesAccessData model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new ModulesAccessData();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'module_id' => $model->module_id, 'url' => $model->url, 'user_id' => $model->user_id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing ModulesAccessData model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $module_id
     * @param string $url
     * @param integer $user_id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($module_id, $url, $user_id)
    {
        $model = $this->findModel($module_id, $url, $user_id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'module_id' => $model->module_id, 'url' => $model->url, 'user_id' => $model->user_id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing ModulesAccessData model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $module_id
     * @param string $url
     * @param integer $user_id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($module_id, $url, $user_id)
    {
        $this->findModel($module_id, $url, $user_id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the ModulesAccessData model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $module_id
     * @param string $url
     * @param integer $user_id
     * @return ModulesAccessData the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($module_id, $url, $user_id)
    {
        if (($model = ModulesAccessData::findOne(['module_id' => $module_id, 'url' => $url, 'user_id' => $user_id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
