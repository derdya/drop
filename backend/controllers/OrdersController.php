<?php

namespace backend\controllers;

use common\models\Products;
use common\models\ProductsOrder;
use Yii;
use common\models\Orders;
use common\models\OrdersSearch;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

class OrdersController extends Controller
{

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        $searchModel = new OrdersSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }


    public function actionCreate()
    {
        $productsModel = new ProductsOrder();
        $model = new Orders();
        if (Yii::$app->request->isPjax) {
            $this->makeOrder($model);
        }
        if (isset($_SESSION['productsInOrder']['id'])) {
            unset($_SESSION['productsInOrder']);
        } else {
            $model->productsInOrder = @$_SESSION['productsInOrder']['productsOrder'];
        }
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        }
        return $this->render('create', [
            'poModel' => $productsModel,
            'model' => $model,
        ]);
    }


    public function actionUpdate($id)
    {
        $productsModel = new ProductsOrder();
        $model = $this->findModel($id);
        if (Yii::$app->request->isPjax) {
            $this->makeOrder($model);
        }

        $model->date_update = time();
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        }
        return $this->render('update', [
            'poModel' => $productsModel,
            'model' => $model,
        ]);
    }


    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        return $this->redirect(['index']);
    }


    protected function findModel($id)
    {
        if (($model = Orders::find()->where(['orders.id' => $id])->with(['products'])->one()) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function makeOrder($model)
    {
        $post = Yii::$app->request->post();
        if (isset($post['product_id'])) {
            $products = Products::find()->where(['id' => $post['product_id']])->one();
            $product = Yii::createObject([
                'class' => ProductsOrder::className(),
                'product_id' => $post['product_id'],
                'name' => $products->name,
                'quantity' => $post['quantity'],
                'seller_price' => $post['seller_price'],
            ]);
            $model->addProduct($product);
        }
        if (isset($post['country_id']) && isset($post['module_id'])) {
            $model->country_id = $post['country_id'];
            $model->module_id = $post['module_id'];
            $model->clsProduct();
        }
        isset($post['delete_id']) ? $model->delProduct($post['delete_id']) : false;
    }
}
