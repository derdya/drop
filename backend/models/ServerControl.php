<?php
/**
 * Created by PhpStorm.
 * User: fbp
 * Date: 02.07.2018
 * Time: 11:03
 */

namespace backend\models;

use Yii;
use yii\base\Model;

class ServerControl extends Model
{
    public $port;

    const SERVERS = ['order' => 9000, 'statistics' => 9001];

    public static function startServer($type = null)
    {
        $pid = self::getServerPid(self::SERVERS[$type]);
        if (empty($pid)) {
            exec("nohup php -q " . $_SERVER['DOCUMENT_ROOT'] . "/yii server/start " . self::SERVERS[$type] . " > /dev/null 2>/dev/null &");
            while (empty($pid)) {
                $pid = self::getServerPid(self::SERVERS[$type]);
            }
            Yii::$app->session->setFlash('success', 'Сервер запущен!');
            return true;
        }
        return false;
    }

    public static function stopServer($type = null)
    {
        $pid = self::getServerPid(self::SERVERS[$type]);
        if (!empty($pid)) {
            exec('kill' . $pid);
            Yii::$app->session->setFlash('success', 'Сервер остановлен!');
        } else {
            Yii::$app->session->setFlash('error', 'Сервер уже остановлен, не клоцай!');
        }
    }

    public static function restartServer()
    {
        self::stopServer();
        Yii::$app->session->setFlash('success', 'Перезапуск сервера!');
        self::startServer();
    }

    public static function getServerPid($port)
    {
        exec("fuser $port/tcp", $pid);
        $pid = !empty($pid) ? $pid[0] : 0;
        return $pid;
    }

    public static function getAllPids()
    {
        $pids = [];
        foreach (self::SERVERS as $key => $port) {
            $pid = 0;
            exec("fuser $port/tcp", $pid);
            $pids[$key] = !empty($pid) ? $pid[0] : 0;
        }
        return $pids;
    }
}