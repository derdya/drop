<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

$this->title = 'Страны';
$this->params['breadcrumbs'][] = $this->title;
$this->params['buttons'] = [
    Html::a('Добавить страну', ['create'], ['class' => 'btn btn-success']),
];
?>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],

        'id',
        'name',
        'code_geoip',
        'delivery',
        'return',
        //'currency_id',

        ['class' => 'yii\grid\ActionColumn'],
    ],
]); ?>
