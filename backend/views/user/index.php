<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\Url;

$this->title = 'Пользователи';
$this->params['breadcrumbs'][] = $this->title;
$this->params['buttons'] = [
    Html::a('Создать пользователя', ['create'], ['class' => 'btn btn-success']),
];
?>


<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],

        'id',
        'username',
//            'auth_key',
//            'password_hash',
//            'password_reset_token',
        'email:email',
        'status',
        'created_at',
        //'updated_at',
//            'group_id',

        ['class' => 'yii\grid\ActionColumn', 'header' => 'Действия',
            'template' => '{view}&nbsp;&nbsp;{update}&nbsp;&nbsp;{permit}&nbsp;&nbsp;{delete}',
            'buttons' =>
                [
                    'permit' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-wrench"></span>', Url::to(['/permit/user/view', 'id' => $model->id]), [
                            'title' => Yii::t('yii', 'Change user role'),
                        ]);
                    },
                ],
        ],
    ],
]); ?>

