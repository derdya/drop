<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>

<div class="user-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'username')->textInput() ?>
    <?= $form->field($model, 'email')->textInput() ?>
    <?= $form->field($model, 'status')->radioList([$model::STATUS_ACTIVE => 'Активный', $model::STATUS_DELETED => 'Неактивный']) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('common', 'buttonSave'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
