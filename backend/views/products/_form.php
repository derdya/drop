<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use mihaildev\ckeditor\CKEditor;
use \yii\widgets\MaskedInput;

?>

<div class="products-form" style="overflow-x: auto">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
    <?= \common\widgets\Alert::widget() ?>
    <?= Html::img('/common/uploads/images/offers/' . $model->img, ['id' => 'imgView', 'style' => 'width: 150px']) ?>
    <?= $form->field($model, 'img')->fileInput() ?>

    <?= $form->field($model, 'code')->textInput() ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->widget(CKEditor::className(), [
        'editorOptions' => [
            'preset' => 'standard', //разработанны стандартные настройки basic, standard, full данную возможность не обязательно использовать
            'inline' => false, //по умолчанию false
            'rows' => 6,
        ],
    ]);/*->textarea(['rows' => 6]) */ ?>

    <?= $form->field($model, 'module_id')
        ->dropDownList(\yii\helpers\ArrayHelper::map(\common\models\Modules::find()->all(), 'id', 'name')) ?>
    <!--    --><? //= Html::dropDownList()?>
    <?= $form->field($model, 'country_id')
        ->dropDownList(\yii\helpers\ArrayHelper::map(\common\models\Country::find()->all(), 'id', 'name')) ?>

    <?= $form->field($model, 'treatment_type_id')
        ->dropDownList(\yii\helpers\ArrayHelper::map(\common\models\ProductsTreatment::find()->all(), 'id', 'name')) ?>

    <?= $form->field($model, 'category_id')
        ->dropDownList(\yii\helpers\ArrayHelper::map(\common\models\Category::find()->all(), 'id', 'name')) ?>

    <?= $form->field($model, 'price')->widget(MaskedInput::className(), [
        'name' => 'price',
        'mask' => '9{1,8}.9{0,2}',
    ])->textInput(['placeholder' => '0.00']) ?>

    <?= $form->field($model, 'rec_price')->widget(MaskedInput::className(), [
        'name' => 'rec_price',
        'mask' => '9{1,8}.9{0,2}',
    ])->textInput(['placeholder' => '0.00']) ?>

    <?= $form->field($model, 'min_price')->widget(MaskedInput::className(), [
        'name' => 'min_price',
        'mask' => '9{1,8}.9{0,2}',
    ])->textInput(['placeholder' => '0.00']) ?>

    <?php is_null($model->private) ? $model->private = 0 : $model->private; ?>
    <?php is_null($model->active) ? $model->active = 0 : $model->active; ?>
    <?= $form->field($model, 'private')->radioList([1 => 'да', 0 => 'нет']) ?>
    <?= $form->field($model, 'active')->radioList([1 => 'да', 0 => 'нет']) ?>
    <?php
    ?>
    <?= $form->field($model, 'productusers')->widget(\nex\chosen\Chosen::className(),
        [
            'items' => \yii\helpers\ArrayHelper::map(\common\models\User::find()->all(), 'id', 'email'),
            'value' => $model->productusers,
            'multiple' => true,
            'disableSearch' => false,
            'placeholder' => 'Пользователи',
            'clientOptions' => [
                'search_contains' => true,
                'single_backstroke_delete' => false,
            ],
        ]
    ) ?>
    <div class="box-footer">
        <div class="form-group pull-right">
            <?= Html::submitButton(\Yii::t('common', 'buttonSave'), ['class' => 'btn btn-success']) ?>
        </div>
    </div>
    <?php ActiveForm::end(); ?>


    <script>
        $(document).ready(function () {
            $("#products-img").change(function () {
                readURL(this);
            });


        });
    </script>
</div>
<?php
$js = <<<JS
$(document).ready(function() {
    if ($('input[name="Products[private]"]:radio:checked').val() == 0){
            $('#products-productusers').prop('disabled', true).trigger("chosen:updated");
    }
    $('input[name="Products[private]"]:radio').on('change', function () {
        if ($(this).val() == 0){
            $('#products-productusers').prop('disabled', true).trigger("chosen:updated");
        }else {
            $('#products-productusers').prop('disabled', false).trigger("chosen:updated");
        }
    });
});
JS;
?>

<?php $this->registerJs($js); ?>
