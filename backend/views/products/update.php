<?php

$this->params['breadcrumbs'][] = ['label' => 'Оффер', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Редактировать(' . $model->name . ')';
?>

<?= $this->render('_form', [
    'model' => $model,
]) ?>
