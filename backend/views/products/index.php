<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\Url;

$this->title = 'Офферы';
$this->params['breadcrumbs'][] = $this->title;
$this->params['buttons'] = [Html::a('Добавить оффер', ['create'], ['class' => 'btn btn-success'])];
?>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        'code',
        [
            'attribute' => 'country_id',
            'value' => function ($model) {
                return $model->country->name;
            },
        ],
        [
            'attribute' => 'img',
            'label' => 'Картинка',
            'format' => 'raw',
            'value' => function ($data) {
                return Html::img('/common/uploads/images/offers/' . $data->img, [
                    'alt' => 'yii2 - картинка в gridview',
                    'style' => 'width:100px;',
                ]);
            },
            'filter' => false,
        ],

        'name',
        'price',
//            'description:ntext',
//            'img:image',
        //
        //'updated_on',

        [
            'attribute' => 'created_on',
            'label' => 'Создано',
            'format' => 'text',
            'value' => function ($data) {
                return Yii::$app->formatter->asDate($data->created_on, 'yyyy-MM-dd H:i:s');
            },
            'filter' => false,
        ],
        [
            'attribute' => 'updated_on',
            'label' => 'Обновлено',
            'format' => 'text',
            'value' => function ($data) {
                return Yii::$app->formatter->asDate($data->updated_on, 'yyyy-MM-dd H:i:s');
            },
            'filter' => false,
        ],

        ['class' => 'yii\grid\ActionColumn', 'header' => 'Действия'],
    ],
]); ?>
