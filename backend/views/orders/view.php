<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\grid\GridView;
use yii\data\ArrayDataProvider;

/* @var $this yii\web\View */
/* @var $model common\models\Orders */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Orders', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="order-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(\Yii::t('common', 'buttonUpdate'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
//            'number',
            'seller_id',
            'client_name',
            'status_id',
            'treatment_type_id',
            'address:ntext',
            'comment:ntext',
            'phone',
            'ttn',
            'date_create',
            'date_send',
            'send',
            [
                'attribute' => 'products',
                'label' => 'Продукты в заказе',
                'format' => 'html',
                'value' => function ($data) {
                    $productsInOrder = [];
                    foreach ($data->productsOrder as $k => $po) {
                        $name = "empty";
                        foreach ($data->products as $prod) {
                            if ($prod->id == $po->product_id) {
                                $name = $prod->name;
                                break;
                            }
                        }
                        $productsInOrder[] = [
                            'name' => $name,
                            'seller_price' => $po->seller_price,
                            'quantity' => $po->quantity,
                            'total' => $po->total,
                        ];


                    }
                    $provider = new ArrayDataProvider([
                        'allModels' => $productsInOrder,
                        'pagination' => [
                            'pageSize' => 10,
                        ],
                        'sort' => [
                            'attributes' => ['name'],
                        ],
                    ]);
                    $products = GridView::widget([
                        'dataProvider' => $provider,
                        'columns' => [
                            ['class' => 'yii\grid\SerialColumn'],
                            [
                                'attribute' => 'name',
                                'label' => 'Наименование:',
                            ],
                            [
                                'attribute' => 'quantity',
                                'label' => 'Количество:',
                            ],
                            [
                                'attribute' => 'seller_price',
                                'label' => 'Цена продажи:',
                            ],
                            'total',
                        ],
                    ]);

                    return $products;
                },
                'filter' => false,
            ],
        ],
    ]) ?>

</div>
