<?php
$this->params['breadcrumbs'][] = ['label' => 'Заказы', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->number, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактирование';
?>
<?= $this->render('_form', [
    'poModel' => $poModel,
    'model' => $model,
]) ?>

<?php

$js = <<<JS
    var conn = new WebSocket('ws://212.109.223.153:9000');
    conn.onopen = function (e) {
        console.log("Connection established!");
        conn.send(JSON.stringify({'action': 'orderEdit', 'number': {$model->number}}));
    };
    conn.onmessage = function (e) {
            var response = JSON.parse(e.data);
            console.log(response);
            if (response.message === "double"){
             $('body').html('<div style="width:100%;' +
              ' height:100%;' +
               ' z-index: 1000;' +
                 'background:black;' +
                 'color:red;' +
                 'font-size: 10em;' +
                  ' position: absolute;' +
                   'text-align: center;' +
                    '">Это заказ уже открыт!</div>');
              window.location = 'index';
            }
    };
JS;
$this->registerJs($js);
?>

