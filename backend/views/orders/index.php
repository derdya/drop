<?php

use yii\helpers\Html;
use yii\grid\GridView;
use \yii\widgets\Pjax;

$uid = Yii::$app->user->id;
$this->title = 'Заказы';
$this->params['breadcrumbs'][] = $this->title;
$this->params['buttons'] = [
    Html::a('Новый заказ', ['create'], ['class' => 'btn btn-success']),
];
?>
<?php

$js = <<<JS
    $('body').tooltip({
        container: 'body',
        selector: '[data-toggle = "tooltip"]'
    });
    $('body').popover({
        selector: '[data-toggle = "popover"]'  
    });
    var conn = new WebSocket('ws://212.109.223.153:9000');
     conn.onopen = function (e) {
        console.log("Connection established!");
        conn.send(JSON.stringify({'uid': {$uid}, 'action': 'orderEditCheck'}));
     };
     
      conn.onmessage = function (e) {
            var response = JSON.parse(e.data);
            console.log(response);
            var table = $(".grid-view table tbody");
            var row = table.find('tr');
             row.each(function(){
                 var td = $(this).find('td');
                 if($.inArray(Number(td.eq(2).text()), response.orders) > -1 && !$(this).hasClass('blocked')){
                     console.log(Number(td.eq(2).text())+'inarr');
                     $(this).addClass('blocked');
                     td.eq(1).children('a').css("display","none");
                     td.eq(1).append("<i class='fa fa-lock'></i>");
                 }else if($.inArray(Number(td.eq(2).text()), response.orders) === -1) {
                     console.log(Number(td.eq(2).text())+'noarr');
                    $(this).removeClass('blocked');
                    td.eq(1).children('a').css("display","inline-block");
                    td.eq(1).children('i').remove();
                 }
             });
      };
      
     
JS;

$this->registerJs($js, yii\web\View::POS_HEAD);
?>
<?php Pjax::begin([
    'timeout' => 50000,
]); ?>
<style>
    div.tooltip {
        max-width: 300px;
    }

    div.tooltip-inner {
        word-wrap: break-word;
        font-size: 0.9em;
        white-space: pre-wrap;
        max-width: 300px;
    }
</style>

<div style="width: 100%; overflow-x: scroll">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'rowOptions' => function ($model) {
            return ['style' => 'background:' . $model->status->color];
        },

        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'class' => 'yii\grid\ActionColumn',
                'header' => 'Действия',
                'contentOptions' => ['style' => 'text-align: center;'],
            ],
            'number',
            [
                'attribute' => 'date_create',
                'value' => function ($model) {
                    return Yii::$app->formatter->asDatetime($model->date_create, 'd.MM.Y H:i');
                },
            ],
            [
                'attribute' => 'date_update',
                'value' => function ($model) {
                    return Yii::$app->formatter->asDatetime($model->date_update, 'd.MM.Y H:i');
                },
            ],
            [
                'attribute' => 'country_id',
                'value' => function ($model) {
                    return @$model->country->name;
                },
            ],
            [
                'attribute' => 'product',
                'enableSorting' => true,
                'format' => 'raw',
                'contentOptions' => ['style' => 'min-width: 150px;'],
                'headerOptions' => ['style' => 'min-width: 150px;'],
                'value' => function ($model) {
                    $productsInOrder = [];
                    foreach ($model->productsOrder as $k => $po) {
                        $name = "empty";
                        foreach ($model->products as $prod) {
                            if ($prod->id == $po->product_id) {
                                $name = $prod->name;
                                break;
                            }
                        }
                        $productsInOrder[] = $name;

                    }

                    return Html::tag('span', count($productsInOrder), [
                            'title' => implode("\n", $productsInOrder),
                            'data-toggle' => 'tooltip',
                            'style' => 'cursor:pointer; position: relative; border: 1px inset indigo; background: #45c417; color: #FFF;border-radius: 10px;padding: 0px 4px 0px 5px;']) .
                        Html::tag('span', @$productsInOrder[0]);
                },
            ],

            'total',

            [
                'attribute' => 'seller_id',
                'value' => function ($model) {
                    return $model->user->username;
                },
            ],

            [
                'attribute' => 'client_name',
                'label' => 'Имя клиента',
            ],

            [
                'attribute' => 'treatment_type_id',
                'label' => 'Тип обработки',
                'contentOptions' => ['style' => 'min-width: 150px;'],
                'headerOptions' => ['style' => 'min-width: 150px;'],
                'value' => function ($model) {
                    return @$model->ordersTreatment->name;
                },
                'filter' => \yii\helpers\ArrayHelper::map(\common\models\OrdersTreatment::find()->all(), 'id', 'name'),
            ],
            [
                'attribute' => 'status_id',
                'label' => 'Статус',
                'contentOptions' => ['style' => 'min-width: 150px;'],
                'headerOptions' => ['style' => 'min-width: 150px;'],
                'value' => function ($model) {
                    return @$model->status->name;
                },
                'filter' => \yii\helpers\ArrayHelper::map(\common\models\Status::find()->all(), 'id', 'name'),
            ],
            [
                'attribute' => 'ttn',
            ],
            'client_name',
            'phone',
            'comment',
            'address',
            'utm_source',
            'utm_medium',
            'utm_campaign',
            'utm_term',
            'utm_content',
            [
                'attribute' => 'ip_address',
                'label' => 'IP',
            ],
        ],
    ]); ?>
</div>
<script>
    if (conn.readyState) {
        conn.send(JSON.stringify({'uid': <?=$uid?>, 'action': 'orderEditCheck'}));
    }
</script>
<?php Pjax::end(); ?>





