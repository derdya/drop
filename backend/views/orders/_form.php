<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\grid\GridView;
use yii\widgets\Pjax;
use common\models\ProductsOrder;
use yii\data\ArrayDataProvider;
use \yii\bootstrap\Modal;

?>
<?= \common\widgets\Alert::widget() ?>
<?php $form = ActiveForm::begin(); ?>

<?= $form->field($model, 'country_id')
    ->dropDownList(\yii\helpers\ArrayHelper::map(\common\models\Country::find()->all(), 'id', 'name'), ['class' => 'field-upd form-control']) ?>

<?= $form->field($model, 'module_id')
    ->dropDownList(\yii\helpers\ArrayHelper::map(\common\models\Modules::find()->all(), 'id', 'name'), ['class' => 'field-upd form-control']) ?>





<?= Html::a('Добавить товар', ['#'], [
    'class' => 'btn btn-success',
    'data-toggle' => 'modal',
    'data-tooltip' => 'true',
    'data-target' => '#productModal',
]);
?>
<? Pjax::begin([
    'id' => 'products-country-depend',
    'timeout' => false,
    'enablePushState' => false,
    'clientOptions' => ['method' => 'POST'],
]); ?>
<?php
Modal::begin([
    'id' => 'productModal',
    'header' => '<h2>Товары</h2>',
]);
?>

<?= $form->field($model, 'productsList')->dropDownList(\yii\helpers\ArrayHelper::map($model->productsList, 'id', 'name')) ?>

<?= $form->field($poModel, 'quantity')->input('number', ['value' => 1, 'min' => 1, 'max' => 9999,]) ?>

<?= $form->field($poModel, 'seller_price')->input('number', ['value' => 1, 'min' => 1, 'max' => 9999,]) ?>

<div class="form-group">
    <?= Html::button('Добавить товар', ['id' => 'addProduct', 'class' => 'btn btn-success']) ?>
</div>

<?php Modal::end(); ?>

<?php
$provider = new ArrayDataProvider([
    'allModels' => $model->productsInOrder,

    'pagination' => [
        'pageSize' => 10,
    ],
    'sort' => [
        'attributes' => ['name'],
    ],
]);
?>
<? Pjax::begin([
    'id' => 'productList',
    'timeout' => false,
    'enablePushState' => false,
    'clientOptions' => ['method' => 'POST'],
]); ?>
<?= GridView::widget([
    'dataProvider' => $provider,
    'showFooter' => true,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        [
            'attribute' => 'name',
            'label' => 'Наименование:',
            'value' => function ($data) {
                return @$data->product->name;
            },
        ],
        [
            'attribute' => 'quantity',
            'label' => 'Количество:',
        ],
        [
            'attribute' => 'seller_price',
            'label' => 'Цена продажи:',
        ],
        [
            'attribute' => 'total',
            'footer' => "<strong>" . $model->getTotal() . "</strong>",
        ],
        [
            'class' => 'yii\grid\ActionColumn',
            'header' => 'Действия',
            'template' => '{delete}',
            'buttons' => [
                'delete' => function ($url, $model, $key) {
                    return Html::a('<span class="glyphicon glyphicon-trash"></span>', '#', [
                        'id' => 'delProduct',
                        'data-id' => $key,
                    ]);
                },

            ],
        ],
    ],
]); ?>

<? Pjax::end(); ?>
<? Pjax::end(); ?>



<?= $form->field($model, 'client_name')->textInput(['maxlength' => true]) ?>

<?= $form->field($model, 'payment_type_id')
    ->dropDownList(\yii\helpers\ArrayHelper::map(\common\models\PaymentType::find()->all(), 'id', 'name'))
?>
<? Pjax::begin([
    'id' => 'option-country-depend',
    'timeout' => false,
    'enablePushState' => false,
    'clientOptions' => ['method' => 'POST'],
]); ?>
<?= $form->field($model, 'delivery_id')
    ->dropDownList(\yii\helpers\ArrayHelper::map($model->deliveryList, 'id', 'name'))
?>
<? Pjax::end(); ?>
<?= $form->field($model, 'treatment_type_id')
    ->dropDownList(\yii\helpers\ArrayHelper::map(\common\models\OrdersTreatment::find()->all(), 'id', 'name')) ?>

<?= $form->field($model, 'address')->textInput(['rows' => 6]) ?>

<?= $form->field($model, 'comment')->textarea(['rows' => 6]) ?>

<?= $form->field($model, 'phone')->widget(\yii\widgets\MaskedInput::className(), [
    'mask' => '(999)999-99-99',
]) ?>

<?= $form->field($model, 'ttn')->textInput(['maxlength' => true]) ?>
<?php is_null($model->send) ? $model->send = 0 : $model->send; ?>
<?= $form->field($model, 'send')->radioList([1 => 'да', 0 => 'нет']) ?>


<div class="form-group">
    <?= Html::submitButton(\Yii::t('common', 'buttonSave'), ['class' => 'btn btn-success']) ?>
</div>

<?php ActiveForm::end(); ?>
<?php
$url = Yii::$app->request->url;
$js = <<<JS

$(document).on('change', '.field-upd', function () {
        var country = $('#orders-country_id').val();
        var module = $('#orders-module_id').val();
        $.pjax.reload({
            type: 'POST',
            data: {
                country_id: country,
                module_id: module
            },
            container: '#option-country-depend',
            replace: false,
            url: '$url'
        }).done(function() {
              $.pjax.reload({
                type: 'POST',
                data: {
                    country_id: country,
                    module_id: module
                },
                container: '#products-country-depend',
                replace: false,
                url: '$url'
              });
        });
    });





    $(document).on('click', '#addProduct', function () {
        var quantity = $('#productsorder-quantity').val();
        var product_id = $('#orders-productslist').val();
        var seller_price = $('#productsorder-seller_price').val();
        if(product_id){
            $.pjax.reload({
                type: 'POST',
                data: {
                    product_id: product_id,
                    quantity: quantity,
                    seller_price: seller_price
                },
                container: '#productList',
                replace: false,
                url: '$url'
            });
            $('#productModal').modal('hide');
        }else{
            alert('Ошибка при добавлениии товара...');
        }
    });
    
    
    
    $(document).on('click', '#delProduct', function (e) {
        var delete_id = $(this).attr('data-id');
        e.preventDefault();
        $.pjax.reload({
            type: 'POST',
            data: {
                delete_id: delete_id,
            },
            container: '#productList',
            replace: false,
            url: '$url'
        });
    });
JS;
?>

<?php $this->registerJs($js); ?>


