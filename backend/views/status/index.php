<?php

use yii\helpers\Html;
use yii\grid\GridView;

$this->title = 'Статусы';
$this->params['breadcrumbs'][] = $this->title;
$this->params['buttons'] = [
    Html::a('Новый статус', ['create'], ['class' => 'btn btn-success']),
];
?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'name',
            'active',
            'color',
            'sort',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
