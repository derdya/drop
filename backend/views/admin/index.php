<?php

use \yii\helpers\Html;
use \yii\widgets\Pjax;
use \common\widgets\Alert;

?>
<?php Pjax::begin(); ?>
<?= Alert::widget(); ?>
    <?php foreach ($pids as $key => $pid): ?>
        <span><b><?= $key ?>: </b></span><?= Html::tag('span', @$pid) ?>
    <?php endforeach; ?>
<?= Html::tag('strong', 'Ваш IP:' . $_SERVER['REMOTE_ADDR'], ['class' => 'row']) ?>
<?= Html::a('Cтарт Order', ['admin/server'], ['data' => ['pjax' => 1, 'method' => 'post', 'params' => ['command' => 'start', 'type' => 'order'],], 'class' => 'btn btn-lg btn-primary',]) ?>
<?= Html::a('Cтарт Statistics', ['admin/server'], ['data' => ['pjax' => 1, 'method' => 'post', 'params' => ['command' => 'start', 'type' => 'statistics'],], 'class' => 'btn btn-lg btn-primary',]) ?>
<?= Html::a('Стоп Order', ['admin/server'], ['data' => ['pjax' => 1, 'method' => 'post', 'params' => ['command' => 'stop', 'type' => 'order'],], 'class' => 'btn btn-lg btn-danger',]) ?>
<?= Html::a('Стоп Statistics', ['admin/server'], ['data' => ['pjax' => 1, 'method' => 'post', 'params' => ['command' => 'stop', 'type' => 'statistics'],], 'class' => 'btn btn-lg btn-danger',]) ?>

<?php Pjax::end() ?>