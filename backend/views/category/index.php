<?php

use yii\helpers\Html;
use yii\grid\GridView;

$this->title = 'Категории';
$this->params['breadcrumbs'][] = $this->title;
$this->params['buttons'] = [
    Html::a('Добавить Категорию', ['create'], ['class' => 'btn btn-success']),
];
?>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],

//            'id',
        'name',

        ['class' => 'yii\grid\ActionColumn', 'header' => 'Действия'],
    ],
]); ?>

