<?php

use yii\helpers\Html;

$this->params['breadcrumbs'][] = ['label' => 'Валюты', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактировать(' . $model->name . ')';
?>

<?= $this->render('_form', [
    'model' => $model,
]) ?>


