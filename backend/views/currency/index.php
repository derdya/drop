<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

$this->title = 'Валюта';
$this->params['breadcrumbs'][] = $this->title;
$this->params['buttons'] = [
    Html::a('Добавить Валюту', ['create'], ['class' => 'btn btn-success btn-sm']),
];
?>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
//                            'id',
        'name',
        'abr',
        ['class' => 'yii\grid\ActionColumn', 'header' => 'Действия'],
    ],
]); ?>
