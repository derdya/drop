<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

$this->title = $model->header;
$this->params['breadcrumbs'][] = ['label' => $model->header, 'url' => ['index']];
$this->params['breadcrumbs'][] = $model->name;
$this->params['buttons'] = [
    Html::a(\Yii::t('common', 'buttonUpdate'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary btn-sm']),
    Html::a(\Yii::t('common', 'buttonDelete'), ['delete', 'id' => $model->id], [
        'class' => 'btn btn-danger btn-sm',
        'data' => [
            'confirm' => \Yii::t('common', 'msgDelConfirm'),
            'method' => 'post',
        ],
    ]),
];
?>

<?= DetailView::widget([
    'model' => $model,
    'attributes' => [
        'id',
        'name',
        'abr',
        [
            'attribute' => 'country_id',
            'value' => function ($data) {
                return $data->country->name;
            },

        ],
    ],
]) ?>
