<?php

$this->params['breadcrumbs'][] = ['label' => 'Валюта', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Добавить ' . $this->title;
?>

<?= $this->render('_form', [
    'model' => $model,
]) ?>
