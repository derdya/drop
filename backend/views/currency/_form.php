<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Currency */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="currency-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'full_name')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'abr')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'country_id')
        ->dropDownList(\yii\helpers\ArrayHelper::map(\common\models\Country::find()->all(), 'id', 'name')) ?>


    <div class="form-group pull-right">
        <?= Html::submitButton(\Yii::t('common', 'buttonSave'), ['class' => 'btn btn-success']) ?>
    </div>


    <?php ActiveForm::end(); ?>

</div>
