<?php

$this->params['breadcrumbs'][] = ['label' => 'Новости', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Редактировать(' . $model->name . ')';
?>
<?= $this->render('_form', [
    'model' => $model,
]) ?>
