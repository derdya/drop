<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Новости', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$this->params['buttons'] = [
    Html::a(\Yii::t('common', 'buttonUpdate'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']),
    Html::a(\Yii::t('common', 'buttonDelete'), ['delete', 'id' => $model->id], [
        'class' => 'btn btn-danger',
        'data' => [
            'confirm' => \Yii::t('common', 'msgDelConfirm'),
            'method' => 'post',
        ],
    ]),
];
?>


</p>

<?= DetailView::widget([
    'model' => $model,
    'attributes' => [
        'id',
        'name',
        'description:ntext',
        'img:ntext',
        'created_on',
        'updated_on',
        'active',
    ],
]) ?>

</div>
