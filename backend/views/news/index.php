<?php

use yii\helpers\Html;
use yii\grid\GridView;

$this->title = 'Новости';
$this->params['breadcrumbs'][] = $this->title;
$this->params['buttons'] = [
    Html::a('Создание новости', ['create'], ['class' => 'btn btn-success']),
];
?>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
//        'id',
//        'description:ntext',
//        'img:ntext',
        [
            'attribute' => 'img',
            'label' => 'Картинка',
            'format' => 'raw',
            'value' => function ($data) {
                return Html::img('/common/uploads/images/news/' . $data->img, [
                    'alt' => 'yii2 - картинка в gridview',
                    'style' => 'width:100px;',
                ]);
            },
            'filter' => false,
        ],
        'name',
//        'created_on',
//        'updated_on',
        'active',

        ['class' => 'yii\grid\ActionColumn', 'header' => 'Действия'],
    ],
]); ?>

