<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use mihaildev\ckeditor\CKEditor;

?>

<div class="news-form">

    <?php $form = ActiveForm::begin([
        'options' => [
            'enctype' => 'multipart/form-data',
        ],
    ]); ?>
    <?= Html::img('/common/uploads/images/news/' . $model->img, ['id' => 'imgView','style' => 'width: 150px']) ?>
    <?= $form->field($model, 'img')->fileInput() ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->widget(CKEditor::className(),[
        'editorOptions' => [
            'preset' => 'standard', //разработанны стандартные настройки basic, standard, full данную возможность не обязательно использовать
            'inline' => false, //по умолчанию false
            'rows' => 6
        ],
    ]);/*->textarea(['rows' => 6])*/ ?>

    <!--    --><? //= $form->field($model, 'created_on')->textInput() ?>

    <!--    --><? //= $form->field($model, 'updated_on')->textInput() ?>

    <?= $form->field($model, 'active')->radioList(['1' => 'да', '0' => 'нет']) ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<script>
    $(document).ready(function () {
        $("#news-img").change(function () {
            readURL(this);
        });
    });
</script>
