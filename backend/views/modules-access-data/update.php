<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\ModulesAccessData */

$this->title = 'Update Modules Access Data: ' . $model->module_id;
$this->params['breadcrumbs'][] = ['label' => 'Modules Access Datas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->module_id, 'url' => ['view', 'module_id' => $model->module_id, 'url' => $model->url, 'user_id' => $model->user_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="modules-access-data-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
