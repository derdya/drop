<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\ModulesAccessData */

$this->title = 'Create Modules Access Data';
$this->params['breadcrumbs'][] = ['label' => 'Modules Access Datas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="modules-access-data-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
