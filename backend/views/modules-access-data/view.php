<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\ModulesAccessData */

$this->title = $model->module_id;
$this->params['breadcrumbs'][] = ['label' => 'Modules Access Datas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="modules-access-data-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'module_id' => $model->module_id, 'url' => $model->url, 'user_id' => $model->user_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'module_id' => $model->module_id, 'url' => $model->url, 'user_id' => $model->user_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'module_id',
            'url:url',
            'api_key',
            'user_id',
        ],
    ]) ?>

</div>
