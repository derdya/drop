<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

?>

<div class="menu-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'parent_id')->dropDownList($model->getParentsList()) ?>

    <?= $form->field($model, 'group_id')->dropDownList(ArrayHelper::map($model->getGroups(), 'id', 'name')) ?>

    <?= $form->field($model, 'url')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'icon')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'label')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'sort')->textInput() ?>

    <?= $form->field($model, 'active')->radioList(['1' => 'да', '0' => 'нет']) ?>
    <div class="box-footer">
        <div class="form-group pull-right">
            <?= Html::submitButton(\Yii::t('common', 'buttonSave'), ['class' => 'btn btn-success']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
