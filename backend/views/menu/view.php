<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Menu */

$this->title = $model->header;
$this->params['breadcrumbs'][] = ['label' => 'Меню', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Просмотр (' . $model->label . ')';
$this->params['buttons'] = [
    Html::a(\Yii::t('common', 'buttonUpdate'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary btn-sm']),
    Html::a(\Yii::t('common', 'buttonDelete'), ['delete', 'id' => $model->id], [
        'class' => 'btn btn-danger btn-sm',
        'data' => [
            'confirm' => \Yii::t('common', 'msgDelConfirm'),
            'method' => 'post',
        ],
    ]),
];
?>


<?= DetailView::widget([
    'model' => $model,

    'attributes' => [
        //'id',
        'parent_id',
        'url',
        'icon',
        'label',
        'sort',
        'active',
        'group_id',
    ],
]) ?>

