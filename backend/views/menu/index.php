<?php

use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\Html;

$this->title = 'Меню';
$this->params['breadcrumbs'][] = $this->title;
$this->params['buttons'] = [
    Html::a('Добавить пункт меню', ['create'], ['class' => 'btn btn-success btn-sm']),
];
?>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn',],

//                            'id',
        'label',
        'url',
//                            'icon',
        [
            'attribute' => 'icon',
            'label' => 'Иконка',
            'format' => 'text', // Возможные варианты: raw, html
            'content' => function ($data) {

                return '<i class="fa fa-' . $data->icon . '"></i> ' . $data->icon;

//
            },
//                                'filter' => $model->getParentsList()
        ],
        [
            'attribute' => 'parent_id',
            'label' => 'Родительская категория',
            'format' => 'text', // Возможные варианты: raw, html
            'content' => function ($data) {
                if (!empty($data->parent->label)) {
                    return $data->parent->label;
                }
//
            },
//                                'filter' => $model->getParentsList()
        ],
        [
            'attribute' => 'group_id',
            'format' => 'text', // Возможные варианты: raw, html
            'content' => function ($data) {
                return $data->group->name;
            },
        ],
        'sort',
        [
            'attribute' => 'active',
            'label' => 'Активно',
            'format' => 'text', // Возможные варианты: raw, html
            'content' => function ($data) {
                if ($data->active == 1) {
                    return '<span class="label label-primary">Да</span>';
                } else {
                    return '<span class="label label-danger">Нет</span>';

                }
            },
            'filter' => [
                1 => 'Да',
                0 => 'Нет',
            ],
        ],
        ['class' => 'yii\grid\ActionColumn',
            'header' => 'Действия',
            'buttonOptions' => [
                'data-pjax' => 1,
            ],
        ],
    ],
]); ?>

