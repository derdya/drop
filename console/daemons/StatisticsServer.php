<?php

namespace app\daemons;

use common\models\Streams;
use common\models\Track;
use consik\yii2websocket\events\WSClientEvent;
use consik\yii2websocket\WebSocketServer;
use Ratchet\ConnectionInterface;

class StatisticsServer extends WebSocketServer
{

    private $streams = [];

    public function init()
    {
        parent::init();

        $this->on(self::EVENT_CLIENT_CONNECTED, function (WSClientEvent $e) {
            $e->client->send(json_encode(['response' => 'connected']));
        });

        $this->on(self::EVENT_CLIENT_DISCONNECTED, function (WSClientEvent $e) {

//            $this->checkDisconnectClient($e->client);
//            $this->sendAll('EVENT_CLIENT_DISCONNECTED');
//            $this->checkDisconnectClient($e->client);
        });
    }


    protected function getCommand(ConnectionInterface $from, $msg)
    {
        $request = json_decode($msg, true);
        return !empty($request['action']) ? $request['action'] : parent::getCommand($from, $msg);
    }

    protected function commandCheckStream(ConnectionInterface $client, $msg)
    {
        $data = json_decode($msg, true);
        $uid = isset($data['client']['uid']) ? $data['client']['uid'] : null;
        $client->uid = $uid;
        if ($uid !== null && !isset($this->streams[$uid][$data['client']['stream']])) {
            $this->streams[$uid][$data['client']['stream']]['click'] = 1;
            $this->streams[$uid][$data['client']['stream']]['connectionTime'] = time();
            $this->streams[$uid][$data['client']['stream']]['ip_address'] = $client->remoteAddress;
            $this->streams[$uid][$data['client']['stream']]['userAgent'] = $data['client']['userAgent'];
            $this->streams[$uid][$data['client']['stream']]['landPrice'] = $this->getPrice($data['client']['stream']);
            $this->streams[$uid][$data['client']['stream']]['landPage'] = $this->getLanding($data['client']['stream']);
        } else {
            $this->streams[$uid][$data['client']['stream']]['click'] += 1;
        }
        $client->send(json_encode(['page' => $this->streams[$uid][$data['client']['stream']]['landPage']]));
        $client->send(json_encode(['lp' => $this->streams[$uid][$data['client']['stream']]['landPrice']]));
    }


    protected function sendAll($msg)
    {
        $data = [
//            'client' => (array)$client,
            'msg' => $msg,
        ];

        foreach ($this->clients as $currentClient) {
            $currentClient->send(json_encode($data));
        }
    }

    protected function checkDisconnectClient($currentClient)
    {
        $countClient = 0;
        foreach ($this->clients as $client) {
            if ($client->uid == $currentClient->uid) {
                $countClient++;
            }
        }
        if ($countClient < 2) {
            foreach ($this->streams[$currentClient->uid] as $stream) {
                $this->sendAll($stream);
//                $track = new Track();
//                $track->date = $currentClient->connectionTime;
//                $track->ip_address = $currentClient->remoteAddress;
//                $track->uid = $currentClient->uid;
//                $track->timeonpage = time() - $currentClient->connectionTime;
//                $track->stream = $currentClient->uid;
//                $track->save();
            }
            unset($this->streams[$client->uid]);
        }
        $this->sendAll($this->streams);
    }

    protected function getPrice($stream)
    {
        $model = Streams::find()->where(['uid' => $stream])->one();
        $priceArray = unserialize($model->prices);
        $lastElem = array_shift($priceArray);
        array_push($priceArray, $lastElem);
        $model->prices = serialize($priceArray);
        $model->save();
        return $lastElem;
    }

    protected function getLanding($stream)
    {
        $landArr = [
            'http://derdya.zzz.com.ua/land1',
            'http://derdya.zzz.com.ua/land2',
            'http://derdya.zzz.com.ua/land3',
        ];
        shuffle($landArr);
        $lastElem = array_shift($landArr);
        return $lastElem;
    }
}