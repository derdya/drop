<?php

namespace app\daemons;

use consik\yii2websocket\events\WSClientEvent;
use consik\yii2websocket\WebSocketServer;
use Ratchet\ConnectionInterface;
use Yii;

class OrdersServer extends WebSocketServer
{
    public function init()
    {
        parent::init();
        $this->on(self::EVENT_CLIENT_CONNECTED, function (WSClientEvent $e) {
            $e->client->name = null;
        });
        $this->on(self::EVENT_CLIENT_DISCONNECTED, function (WSClientEvent $e) {
            unset($e->client->order);
            $orders = $this->collectOrders();
            $this->sendAll($e->client, ['orders' => $orders]);
        });
    }


    protected function getCommand(ConnectionInterface $from, $msg)
    {
        $request = json_decode($msg, true);
        return !empty($request['action']) ? $request['action'] : parent::getCommand($from, $msg);
    }

    public function commandOrderEdit(ConnectionInterface $client, $msg)
    {
        $request = json_decode($msg, true);
        if (in_array($request['number'], $this->collectOrders())) {
            $client->send(json_encode(['message' => 'double']));
            $client->close();
            return false;
        }
        $client->order = $request['number'];
        $orders = $this->collectOrders();
        $this->sendAll($client, ['orders' => $orders]);
        return true;
    }

    public function commandOrderEditCheck(ConnectionInterface $client, $msg)
    {
        $orders = $this->collectOrders();
        $client->send(json_encode(['orders' => $orders]));
    }

    protected function collectOrders()
    {
        $orders = [];
        foreach ($this->clients as $client) {
            if (isset($client->order)) {
                $orders[] = $client->order;
            }
        }
        return $orders;
    }

    protected function sendAll($client, $data)
    {
        foreach ($this->clients as $chatClient) {
            $chatClient->send(json_encode($data));
        }
        $client->send(json_encode($data));
    }


}