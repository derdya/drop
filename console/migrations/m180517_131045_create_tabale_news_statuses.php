<?php

use yii\db\Migration;

/**
 * Class m180517_131045_create_tabale_news_statuses
 */
class m180517_131045_create_tabale_news_statuses extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{news_statuses}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'description' => $this->string(),
            'date_create' => $this->bigInteger(),
        ]);
        $this->insert('news_statuses', [
            'name' => 'Новый оффер',
            'description' => 'Новый оффер',
            'date_create' => time(),
        ]);$this->insert('news_statuses', [
            'name' => 'Оффер приостановлен',
            'description' => 'Оффер приостановлен',
            'date_create' => time(),
        ]);$this->insert('news_statuses', [
            'name' => 'Оффер возобновлен',
            'description' => 'Оффер возобновлен',
            'date_create' => time(),
        ]);
        $this->insert('news_statuses', [
            'name' => 'Новый оффер',
            'description' => 'Изменены условия',
            'date_create' => time(),
        ]);
    }


    public function safeDown()
    {
        $this->dropTable('{{news_statuses}}');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180517_131045_create_tabale_news_statuses cannot be reverted.\n";

        return false;
    }
    */
}
