<?php

use yii\db\Migration;

/**
 * Handles the creation of table `streams`.
 */
class m180716_082405_create_streams_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('streams', [
            'id' => $this->primaryKey(),
            'uid' => $this->string()->unique(),
            'prices' => $this->text(),
        ], $tableOptions);
        $this->insert('streams', [
            'uid' => uniqid(),
            'prices' => serialize([22, 33, 44]),
        ]);
    }


    public function safeDown()
    {
        $this->dropTable('streams');
    }
}
