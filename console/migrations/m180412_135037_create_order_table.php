<?php

use yii\db\Migration;

/**
 * Handles the creation of table `order`.
 */
class m180412_135037_create_order_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('orders', [
            'id' => $this->primaryKey(),
            'seller_id' => $this->smallInteger(8),
            'client_name' => $this->string(),
            'status_id' => $this->smallInteger(8),
            'process_id' => $this->smallInteger(8),
            'address' => $this->text(),
            'comment' => $this->text(),
            'phone' => $this->string(),
            'ttn' => $this->string(),
            'date_created' => $this->timestamp(),
            'date_send' => $this->timestamp(),
            'send' => $this->boolean(),
        ],$tableOptions);

        $this->addForeignKey(
            'fk-order-status_id',
            'orders',
            'status_id',
            'status',
            'id',
            'CASCADE'
        );
        $this->addForeignKey(
            'fk-order-process_id',
            'orders',
            'process_id',
            'process',
            'id',
            'CASCADE'
        );
        $this->addForeignKey(
            'fk-order-seller_id',
            'orders',
            'seller_id',
            'user',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('orders');
    }
}
