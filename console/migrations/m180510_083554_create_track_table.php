<?php

use yii\db\Migration;

/**
 * Handles the creation of table `track`.
 */
class m180510_083554_create_track_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('track', [
            'id' => $this->primaryKey(),
            'date' => $this->string(),
            'timeonpage' => $this->integer(),
            'ip_address' => $this->string(),
            'uid' => $this->string(),
            'stream' => $this->string(),
            'user_agent' => $this->string(),
            'transition' => $this->boolean()->defaultValue(0),
            'land_price' => $this->integer(),
        ]);
    }

    public function safeDown()
    {
        $this->dropTable('track');
    }
}
