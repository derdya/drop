<?php

use yii\db\Migration;

/**
 * Handles the creation of table `streams_landing`.
 */
class m180716_091003_create_streams_landing_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('streams_landing', [
            'id' => $this->primaryKey(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('streams_landing');
    }
}
