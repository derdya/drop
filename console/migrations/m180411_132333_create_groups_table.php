<?php

use yii\db\Migration;

/**
 * Handles the creation of table `groups`.
 */
class m180411_132333_create_groups_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('groups', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
        ],$tableOptions);
        $this->alterColumn('groups', 'id', $this->smallInteger(8).'NOT NULL AUTO_INCREMENT');

        $this->insert('groups', [
            'name' => 'admin',
        ]);
        $this->insert('groups', [
            'name' => 'seller',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('groups');
    }
}
