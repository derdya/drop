<?php

use yii\db\Migration;

/**
 * Handles the creation of table `currency`.
 */
class m180413_090000_create_currency_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('currency', [
            'id' => $this->primaryKey(),
            'full_name' => $this->string(50),
            'name' => $this->string(50),
            'abr' => $this->string(5),
            'country_id' => $this->integer(),
        ]);

        $this->addForeignKey(
            'fk_currency_country_id',
            'currency',
            'country_id',
            'country',
            'id',
            'CASCADE'
        );
        $this->insert('currency', [
            'full_name' => 'Гривна',
            'name' => 'грн.',
            'abr' => 'UAH',
            'country_id' => 1,
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('currency');
    }
}
