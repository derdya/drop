<?php

use yii\db\Migration;

/**
 * Handles the creation of table `status`.
 */
class m180412_115245_create_status_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('status', [
            'id' => $this->primaryKey(),
            'name'=> $this->string(),
            'active' => $this->boolean(),
            'color' => $this->string()
        ],$tableOptions);
        $this->alterColumn('status', 'id', $this->smallInteger(8).'NOT NULL AUTO_INCREMENT');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('status');
    }
}
