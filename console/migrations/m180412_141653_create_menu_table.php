<?php

use yii\db\Migration;

/**
 * Handles the creation of table `menu`.
 */
class m180412_141653_create_menu_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('menu', [
            'id' => $this->primaryKey(),
            'parent_id' => $this->smallInteger(8),
            'link' => $this->text(),
            'label' => $this->text(),
            'icon' => $this->text(),
            'active' => $this->boolean(),
        ],$tableOptions);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('menu');
    }
}
