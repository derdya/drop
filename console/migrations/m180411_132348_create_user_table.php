<?php

use yii\db\Migration;

/**
 * Handles the creation of table `user`.
 */
class m180411_132348_create_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%user}}', [
            'id' => $this->primaryKey(),
            'username' => $this->string()->notNull()->unique(),
            'auth_key' => $this->string(32)->notNull(),
            'password_hash' => $this->string()->notNull(),
            'password_reset_token' => $this->string()->unique(),
            'email' => $this->string()->notNull()->unique(),
            'status' => $this->smallInteger()->notNull()->defaultValue(10),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
            'group_id' =>  $this->smallInteger(8),
        ], $tableOptions);
        $this->alterColumn('user', 'id', $this->smallInteger(8).'NOT NULL AUTO_INCREMENT');

        $this->createIndex(
            'idx-user-group_id',
            'user',
            'group_id'
        );

        $this->addForeignKey(
            'fk-user-group_id',
            'user',
            'group_id',
            'groups',
            'id',
            'CASCADE'
        );

        $this->insert('user', [
            'username' => 'admin',
            'email' => 'admin@admin.com',
            'password_hash' => Yii::$app->security->generatePasswordHash('admin')
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('user');
    }
}
