<?php

use yii\db\Migration;

/**
 * Handles the creation of table `streams_transit`.
 */
class m180716_091020_create_streams_transit_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('streams_transit', [
            'id' => $this->primaryKey(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('streams_transit');
    }
}
