<?php

use yii\db\Migration;

/**
 * Handles the creation of table `news`.
 */
class m180424_095000_create_news_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('news', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'description' => $this->text(),
            'img' => $this->text(),
            'created_on' => $this->integer(11),
            'updated_on' => $this->integer(11),
            'active' => $this->boolean(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('news');
    }
}
