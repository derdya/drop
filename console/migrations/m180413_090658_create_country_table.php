<?php

use yii\db\Migration;

/**
 * Handles the creation of table `country`.
 */
class m180413_090658_create_country_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('country', [
            'id' => $this->primaryKey(),
            'name' => $this->string(50),
            'code_geoip' => $this->string(3),
            'delivery' => $this->integer(11),
            'return' => $this->integer(11),

        ], $tableOptions);

        $this->insert('country', [
            'name' => 'Украина',
            'code_geoip' =>'UA',
            'delivery' => '40',
            'return' => '80',
        ]);

    }

    public function safeDown()
    {
        $this->dropTable('country');
    }
}
