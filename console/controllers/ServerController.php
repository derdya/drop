<?php

namespace console\controllers;

use app\daemons\OrdersServer;
use app\daemons\StatisticsServer;
use consik\yii2websocket\WebSocketServer;
use yii\console\Controller;

class ServerController extends Controller
{
    public function actionStart($port = 9000)
    {

        if ($port == 9000) {
            $server = new OrdersServer();
            $server->port = $port; //This port must be busy by WebServer and we handle an error
        } elseif ($port == 9001) {
            $server = new StatisticsServer();
            $server->port = $port; //This port must be busy by WebServer and we handle an error
        }
        $server->on(WebSocketServer::EVENT_WEBSOCKET_OPEN_ERROR, function ($e) use ($server) {
            echo "Error opening port " . $server->port . "\n";
//            $server->port += 1; //Try next port to open
            $server->start();
        });

        $server->on(WebSocketServer::EVENT_WEBSOCKET_OPEN, function ($e) use ($server) {
            echo "Server started at port " . $server->port;
        });

        $server->start();
    }
}